const functions = require('firebase-functions');
const admin = require('firebase-admin');
const serviceAccount = require("./serviceAccountKey.json");
const axios = require('axios')
const convert = require('xml-js')

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://podcast-b079f.firebaseio.com"
});
const db = admin.database()

const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const router = express.Router();

// app.use(bodyParser);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

router.get('/healthcheck', (req, res, next) => {
    res.status(200).send('Response ok :' + new Date())
})


router.get('/channels/:channel/episodes/:ep', (req, res, next) => {
    try {
        db.ref(`episodes/${req.params.channel}/${req.params.ep}`).once("value", snap => {
            res.json(snap.val());
        })
    } catch (e) {
        res.status(404).send("not found")
    }
})

router.get('/channels/:channel', async (req, res, next) => {
    try {
        db.ref(`episodes/${req.params.channel}`).once("value", snap => {
            res.json(snap.val());
        })
    } catch (e) {
        res.status(404).send("not found")
    }
})

router.get('/channels', async (req, res, next) => {
    try {
        db.ref(`channels`).once("value", snap => {
            res.json(snap.val());
        })
    } catch (e) {
        res.status(404).send("not found")
    }
})



router.post('/process', async (req, res, next) => {
    console.log("req.body.content")
    console.log(req.body.content)

    getPodbean(req.body.content).then(res => {
        console.log(JSON.stringify(res))
        // console.log(res["atom:link"]._attributes.href.split('/')[2].split('.')[0])
        let channelName =res["atom:link"]._attributes.href.split('/')[2].split('.')[0]
        let authorEmail =res.owner["itunes:email"]._text
        let authorName =res.owner["itunes:name"]._text
        let channelCategory =res.category[0]._attributes.text
        let description =res["description"]._text
        let imageUrl =res["image"].url._text
        let language =res["language"]._text
        let link =res.link._text
        let title =res["title"]._text

        let channel = {
            key : channelName,
            value : {
                author: {
                    email : authorEmail,
                    name : authorName
                },
                category: channelCategory,
                description: description,
                image: imageUrl,
                language: language,
                link: link,
                title: title
            }
        };
        console.log("complete")
        console.log(channel)
        db.ref('channels').child(channel.key).set(channel.value)
        db.ref('episodes').child(channel.key).set(tranfromItems(res.item))
        return;
    }).catch(e=>{ console.log(e)})
})


exports.api = functions.https.onRequest(router)

// utils
function getPodbean(source) {
    console.log(source)
    return new Promise((reslove, reject) => {
        axios.get(source).then((result) => {
            const data = JSON.parse(convert.xml2json(result.data, { compact: true, spaces: 4 }))
            data.rss.channel = removeItunesFromKey(data.rss.channel)
            data.rss.channel.item = data.rss.channel.item.map(item => removeItunesFromKey(item))
            reslove(data.rss.channel)
            return data.rss.channel
        }).catch((err) => {
            console.log(err)
            reject(err)
        });
    })
}

function removeItunesFromKey(item) {
    const data = {}
    Object.keys(item).forEach(k => {
        const key = k.replace('itunes:', '')
        data[key] = item[k]
    })
    return data
}


function tranfromItems(item) {
    return item.reduce((prev, curr, currentIndex) => {
        const i = getItem(curr, currentIndex)
        prev[i.key] = i.value
        return prev
    }, {})
}

function getItem(data, index) {
    return {
        key: index,
        value: {
            enclosure: {
                type: data.enclosure._attributes.type,
                url: data.enclosure._attributes.url
            },
            duration: data.duration._text,
            image: data.image._attributes.href,
            pubDate: data.pubDate._text,
            subtitle: data.subtitle._text,
            title: data.title._text
        },
    }
}