const axios = require('axios')
const convert = require('xml-js')

function getPodbean(source) {
    return new Promise((reslove, reject) => {
        axios.get(source).then((result) => {
            const data = JSON.parse(convert.xml2json(result.data, { compact: true, spaces: 4 }))
            data.rss.channel = removeItunesFromKey(data.rss.channel)
            data.rss.channel.item = data.rss.channel.item.map(item => removeItunesFromKey(item))
            reslove(data.rss.channel)
            return data.rss.channel
        }).catch((err) => {
            console.log(err)
            reject(err)
        });
    })
}

function removeItunesFromKey(item) {
    const data = {}
    Object.keys(item).forEach(k => {
        const key = k.replace('itunes:', '')
        data[key] = item[k]
    })
    return data
}

getPodbean("https://feed.podbean.com/knd/feed.xml").then(res => {
    console.log(tranfromItems(res.item))
    return
}).catch(err => {
    console.log(err)
})

function tranfromItems(item) {
    return item.reduce((prev, curr, currentIndex) => {
        const i = getItem(curr, currentIndex)
        prev[i.key] = i.value
        return prev
    }, {})
}

function getItem(data, index) {
    return {
        key: index,
        value: {
            enclosure: {
                type: data.enclosure._attributes.type,
                url: data.enclosure._attributes.url
            },
            duration: data.duration._text,
            image: data.image._attributes.href,
            pubDate: data.pubDate._text,
            subtitle: data.subtitle._text,
            title: data.title._text
        },
    }
}
