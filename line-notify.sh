#!/bin/bash
REPO_NAME="$1"
COMMIT_NUMBER="$2"
STATUS_CODE="$3"
LINE_TOKEN="$4"

# Sticker Set
# https://devdocs.line.me/files/sticker_list.pdf

STICKER_FAIL_SET=(519 520 173 23 152)
F_STICKER=${#STICKER_FAIL_SET[@]}
F_INDEX=$(($RANDOM % $F_STICKER))

STICKER_DONE_SET=(144 179 511 158 140)
D_STICKER=${#STICKER_DONE_SET[@]}
D_INDEX=$(($RANDOM % $D_STICKER))

line_notify_sticker()
{
  LINE_TOKEN="$1"
  STICKER_PACKAGE="$2"
  STICKER_ID="$3"
  curl --silent -X POST -H "Authorization: Bearer $LINE_TOKEN" -F "message=$MESSAGE" -F "stickerPackageId=$STICKER_PACKAGE" -F "stickerId=$STICKER_ID" https://notify-api.line.me/api/notify
}
echo "Send pipeline result to LINE Notify"
if [ "$STATUS_CODE" = "0" ]
then
  MESSAGE="✅ [$REPO_NAME:${COMMIT_NUMBER}] Deployment Successfully. สำเร็จแล้ว"
  line_notify_sticker $LINE_TOKEN 2 ${STICKER_DONE_SET[$D_INDEX]}
else
  MESSAGE="🔥 [$REPO_NAME:${COMMIT_NUMBER}] Deployment Failed!"
  line_notify_sticker $LINE_TOKEN 2 ${STICKER_FAIL_SET[$F_INDEX]}
fi
echo "End of script..."