import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Player from "./views/Player";
import PodcastDetailPage from './views/PodcastDetailPage.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/player',
      name: 'player',
      component: Player
    },
    {
      path: '/detail',
      name: 'detail',
      component: PodcastDetailPage,
    }
  ]
})
