import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    content: [
      {
        feedUrl: 'https://feed.podbean.com/knd/feed.xml',
        item: [],
      },
    ],
  },
  mutations: {

  },
  actions: {

  }
})
