import axios from 'axios'
import convert from 'xml-js'

export async function getPodbean (source) {
    const res = await axios.get(source)
    const data = JSON.parse(convert.xml2json(res.data, {compact: true, spaces: 4}))
    data.rss.channel = removeItunesFromKey(data.rss.channel)
    data.rss.channel.item = data.rss.channel.item.map(item => removeItunesFromKey(item))
    return data.rss.channel
}

export function removeItunesFromKey(item) {
    const data = {}
    Object.keys(item).forEach(k => {
        const key = k.replace('itunes:', '')
        data[key] = item[k]
    })
    return data
}